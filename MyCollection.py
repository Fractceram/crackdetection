#######################
# Libraries
#######################
import os
import random
import cv2
import numpy as np
import skimage
from skimage import io
import imageio
from libtiff import TIFF
import matplotlib.pyplot as plt
#######################
# Directories
#######################
DIR_IMG = os.path.join(os.getcwd(), "img")
DIR_NO_SHOCK = os.path.join(DIR_IMG, "0-no-shock")
DIR_300 = os.path.join(DIR_IMG, "1-300")
DIR_600 = os.path.join(DIR_IMG, "2-600")
DIR_900 = os.path.join(DIR_IMG, "3-900")
DIR_ANNOTATED = os.path.join(DIR_IMG, "4-annotated")
DIR_CALIBRATION = os.path.join(DIR_IMG, "4-Kalibrierung")


#######################
# Functions
#######################
def show_images(image_name_tuples, rows=1, cols=2, size=(10, 10)):
	'''
	Display images and its labels.
	:param image_name_tuples: tupel of images and its labels
	:param rows: nr of rows
	:param cols: nr of cols
	:param size: fig size
	'''

	plt.figure(1, figsize=size)
	for i, image_name in enumerate(image_name_tuples):
		img, name = image_name
		c_map = None if img.shape[-1] == 3 else plt.cm.gray
		plt.subplot(rows, cols, i+1)
		plt.imshow(img, cmap=c_map)
		# plt.xticks([]), plt.yticks([])
		plt.title(name)
	return


def get_random_img_from_dir(dir, cvtColor = None):
	file = random.choice(os.listdir(dir))
	filepath = os.path.join(dir, file)
	img = cv2.imread(filepath, cv2.IMREAD_UNCHANGED)
	img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
	return img
